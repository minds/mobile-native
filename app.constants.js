/**
 * The app version
 * This should be updated after each release
 */
const CONSTANTS = {
  APP_VERSION: '5.5.1',
};

module.exports = CONSTANTS;
